import { WhereAttributeHash } from 'sequelize';

export interface IFindAll extends WhereAttributeHash {
    type: string;
    itemId?: string;
    title?: any;
}
