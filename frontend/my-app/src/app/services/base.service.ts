import { HttpClient } from "@angular/common/http";
import { inject } from "@angular/core";

export class BaseService {
  protected http = inject(HttpClient);
  protected baseUrl = 'https://api.sashaexam.ru';
}