import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { Item } from '../types/Item';

@Injectable({
  providedIn: 'root'
})
export class ItemsService extends BaseService {

  constructor() {
    super();
  }

  public getAllItems() {
    return this.http.get<Item[]>(`${this.baseUrl}/api/items`);
  }
}
