import { Component, computed, inject, signal } from '@angular/core';
import { DemoTestQuestion } from '../../types/DemoTestQuestion';
import { DemoTestsService } from '../../services/demo-tests.service';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { DemoTest } from '../../types/DemoTest';
import { DemoTestQuestionComponent } from "../../components/demo-test-question/demo-test-question.component";
import { DemoTestQuestionsBlockComponent } from "../../components/demo-test-questions-block/demo-test-questions-block.component";
import { ViewportScroller } from '@angular/common';
import { DemoTestQuestionAnswer } from '../../types/DemoTestQuestionAnswer';

@Component({
    selector: 'app-demo-test-questions',
    standalone: true,
    templateUrl: './demo-test-questions.component.html',
    styleUrl: './demo-test-questions.component.scss',
    imports: [DemoTestQuestionComponent, DemoTestQuestionsBlockComponent, RouterModule]
})
export class DemoTestQuestionsComponent {
  demoTestService = inject(DemoTestsService);
  router = inject(Router);
  
  demoTestId = this.aroute.snapshot.params['id'];
  demoTest = signal<DemoTest | null>(null);
  
  questions = signal<DemoTestQuestion[]>([]);
  userAnswers = signal<{questionId: string, answerId: string | null, isCorrect: boolean}[]>([]);
  rightAnswers = computed(() => this.userAnswers().filter(x => x.isCorrect).length)

  percentage = computed(() => {
    return this.rightAnswers() / this.questions().length * 100;
  });

  step = signal(0);

  constructor(private aroute: ActivatedRoute, private scroll: ViewportScroller) {
    this.demoTestService.getDemoTestQuestions(this.demoTestId).subscribe(res => {
      this.questions.set(res);

      this.questions().forEach(q => {
        this.userAnswers.update(values => {
          return [...values, {questionId: q.id, answerId: null, isCorrect: false}];
        });
      });

    });
    this.demoTestService.getDemoTestById(this.demoTestId).subscribe(res => {
      this.demoTest.set(res);
    });
  }

  answerQuestion(data: DemoTestQuestionAnswer) {
    this.userAnswers.update(values => {
      let newValues = [...values];
      let question = newValues.find(x => x.questionId === data.questionId);
      question!.isCorrect = data.isCorrect;
      question!.answerId = data.id;
      return newValues;
    });
  }

  findUserAnswer(questionId: string) {
    return this.userAnswers().find(x => x.questionId === questionId)?.answerId as string | null;
  }

  scrollToQuestion(qId: string) {
    this.scroll.scrollToAnchor(qId);
  }

  reload() {
    this.userAnswers.set([]);
    this.questions().forEach(q => {
      this.userAnswers.update(values => {
        return [...values, {questionId: q.id, answerId: null, isCorrect: false}];
      });
    });

    this.step.set(0);
  }

  seeErrors() {
    this.step.set(2);
  }

  ready() {
    this.step.set(1);
  }
}
