import { Component, computed, inject, signal } from '@angular/core';
import { DemoTest, DemoTestFilter, SimpleDemoTest } from '../../types/DemoTest';
import { Observable, Subject, Subscription, combineLatest, debounceTime, distinctUntilChanged, takeUntil } from 'rxjs';
import { DemoTestsService } from '../../services/demo-tests.service';
import { Item } from '../../types/Item';
import { DemoTestComponent } from "../../components/demo-test/demo-test.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ItemsService } from '../../services/items.service';

@Component({
    selector: 'app-demo-tests',
    standalone: true,
    templateUrl: './demo-tests.component.html',
    styleUrl: './demo-tests.component.scss',
    imports: [DemoTestComponent, CommonModule, FormsModule]
})
export class DemoTestsComponent {
  demoTestsService = inject(DemoTestsService);
  itemsService = inject(ItemsService);

  total = computed(() => this.data().length);

  items = signal<Item[]>([]);
  showItems = signal(false);

  currentOrder = signal<'asc' | 'desc'>('desc');

  currentItem: string | null = null;

  filter = signal<DemoTestFilter>({
    type: "ОГЭ",
    itemId: null,
    search: null
  }); 

  types = ['ОГЭ', 'ЕГЭ'];

  searchDebouncer$ = new Subject();
  search = '';

  destroy$ = new Subject<void>();

  data = signal<SimpleDemoTest[]>([]);
  dataRequest$ = this.demoTestsService.getAllDemoTests(this.filter()).pipe(takeUntil(this.destroy$)).subscribe(res => {
    this.data.set(res);
  });

  constructor() {
    this.itemsService.getAllItems().subscribe(res => {
      this.items.set(res);
    })

    this.searchDebouncer$.pipe(distinctUntilChanged(), debounceTime(600)).subscribe(val => {
      this.filter.update(value => {
        return {...value, search: val} as DemoTestFilter;
      });

      this.getDemoTests();
    })
  }

  getDemoTests() {
    this.dataRequest$.unsubscribe();
    this.dataRequest$ = this.demoTestsService.getAllDemoTests(this.filter()).pipe(takeUntil(this.destroy$)).subscribe(res => {
      this.data.set(res);
    });
  }

  sortTests(order: 'asc' | 'desc') {
    this.currentOrder.set(order);
    this.data.update(values => {
      return [...values.sort((a, b) => order === 'asc' ? a.dateCreated.localeCompare(b.dateCreated) : b.dateCreated.localeCompare(a.dateCreated))];
    })
  }

  setItem(item: Item) {
    this.filter.update(value => {
      return {...value, itemId: item.id}
    });

    this.getDemoTests();
    this.showItems.set(false);
  }

  setType(type: string) {
    this.filter.update(value => {
      return {...value, type};
    });

    this.getDemoTests();
  }
}
