import { Component, Inject, computed, inject, signal } from '@angular/core';
import { DemoTest } from '../../types/DemoTest';
import { DemoTestsService } from '../../services/demo-tests.service';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { DemoTestDialogComponent } from '../../components/dialogs/demo-test-dialog/demo-test-dialog.component';
import { DialogData } from '../../types/DialogData';

@Component({
  selector: 'app-demo-test-page',
  standalone: true,
  imports: [RouterModule, MatDialogModule],
  templateUrl: './demo-test-page.component.html',
  styleUrl: './demo-test-page.component.scss'
})
export class DemoTestPageComponent {
  demoTestsService = inject(DemoTestsService);
  dialog = inject(MatDialog);
  router = inject(Router);

  demoTest = signal<DemoTest | null>(null);
  demoTestId = this.aroute.snapshot.params['id'];

  data = computed(() => {
    return {
      title: this.demoTest()?.title ?? '',
      desc: this.demoTest()?.instruction ?? '',
      actions: [
        {
          text: 'Ясно, начать тест',
          returnType: true,
          main: true
        }
      ]
    }
  });

  constructor(private aroute: ActivatedRoute) {
    this.demoTestsService.getDemoTestById(this.demoTestId).subscribe(res => {
      this.demoTest.set(res);
    });
  }

  start() {
    this.dialog.open(DemoTestDialogComponent, {
      data: this.data()
    }).afterClosed().subscribe(res => {
      if (res) {
        this.router.navigate([`/demo-tests/${this.demoTestId}/quest`])
      }
    })
  }
}
