import { Component, EventEmitter, Input, Output, computed, signal } from '@angular/core';
import { DemoTestQuestion } from '../../types/DemoTestQuestion';
import { DemoTestQuestionAnswer } from '../../types/DemoTestQuestionAnswer';

@Component({
  selector: 'app-demo-test-question',
  standalone: true,
  imports: [],
  templateUrl: './demo-test-question.component.html',
  styleUrl: './demo-test-question.component.scss'
})
export class DemoTestQuestionComponent {
  @Input() question!: DemoTestQuestion;
  @Input() userAnswerId: string | null = null;
  @Input() showCorrect: boolean = false;
  @Output() onChoose = new EventEmitter<DemoTestQuestionAnswer>();

  chosenAnswer = signal<string | null>(null);
  get correctAnswer() {
    return this.question.answers.find(x => x.isCorrect)?.text;
  }

  choose(answer: DemoTestQuestionAnswer) {
    this.chosenAnswer.set(answer.id);
    this.onChoose.emit(answer);
  }
}
