import { Component, EventEmitter, Input, Output, output } from '@angular/core';
import { DemoTestQuestion } from '../../types/DemoTestQuestion';

@Component({
  selector: 'app-demo-test-questions-block',
  standalone: true,
  imports: [],
  templateUrl: './demo-test-questions-block.component.html',
  styleUrl: './demo-test-questions-block.component.scss'
})
export class DemoTestQuestionsBlockComponent {
  @Input() questions!: DemoTestQuestion[];
  @Output() onScroll = new EventEmitter<string>();
  @Output() onReady = new EventEmitter();

  scroll(qId: string) {
    this.onScroll.emit(qId);
  }
}
